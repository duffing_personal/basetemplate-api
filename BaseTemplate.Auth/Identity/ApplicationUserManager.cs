﻿using System;
using BaseTemplate.Auth.Modules;
using BaseTemplate.Auth.Stores;
using BaseTemplate.Entities.IdentityEntities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;

namespace BaseTemplate.Auth.Identity
{
    /// <summary>
    ///  Configures the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    /// </summary>
    public class ApplicationUserManager : UserManager<Account, int>
    {
        /// <summary>
        ///  Standard constructor.
        /// </summary>
        /// <param name="store">Store that the usermanager uses.</param>
        public ApplicationUserManager(IUserStore<Account, int> store) : base(store)
        {
            /* An email service needs to be configured before confirmation and password reset emails can be sent out. */
            EmailService = new EmailModule();
        }

        /// <summary>
        ///  Creates the ApplicationUserManager that is used for the application's lifetime.
        /// </summary>
        /// <param name="options">Options that are used for the configuration of the ApplicationUserManager.</param>
        /// <param name="context">OwinContext that's used for the ApplicationUserManager.</param>
        /// <returns>The ApplicationUserManager object used for the application.</returns>
        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            ApplicationUserManager manager = new ApplicationUserManager(new AppUserStore(context.Get<ApiContext>()));

            /* Configure validation logic for usernames. */
            manager.UserValidator = new UserValidator<Account, int>(manager)
            {
                AllowOnlyAlphanumericUserNames = false, // we don't want spaces in usernames.
                RequireUniqueEmail = true // emails for users need to be unique. this is because we force confirmation of emails and we don't want malicious users using other people's emails.
            };

            IDataProtectionProvider dataProtectionProvider = options.DataProtectionProvider;

            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider= new DataProtectorTokenProvider<Account, int>(dataProtectionProvider.Create("ASP.NET Identity"))
                {
                    TokenLifespan = TimeSpan.FromHours(24) // any token created will expire in 24 hours
                };
            }

            return manager;
        }
    }
}