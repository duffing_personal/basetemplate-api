using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using BaseTemplate.Auth.Modules;
using BaseTemplate.Entities.DataEntities;
using BaseTemplate.Entities.Models;

namespace BaseTemplate.Auth.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApiContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApiContext context)
        {
            if (context.Clients.Count() > 0)
            {
                return;
            }

            context.Clients.AddRange(BuildClientsList());
            context.SaveChanges();
        }

        private static List<Client> BuildClientsList()
        {
            List<Client> ClientsList = new List<Client>
            {
                new Client
                {
                    Id = "ngAuthApp",
                    Secret = HelperModule.GetSecret(),
                    Name = "AngularJS front-end Application",
                    ApplicationType = ApplicationTypes.JavaScript,
                    Active = true,
                    RefreshTokenLifeTime = 7200,
                    AllowedOrigin = Properties.Settings.Default.SiteURL
                }
            };

            return ClientsList;
        }
    }
}