﻿using System.Data.Entity;
using BaseTemplate.Auth.Migrations;
using BaseTemplate.Entities.DataEntities;
using BaseTemplate.Entities.IdentityEntities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BaseTemplate.Auth
{
    /// <summary>
    ///  Represents a class that uses the default entity types for ASP.NET Identity Users, Roles, Claims, Logins.
    /// </summary>
    public class ApiContext : IdentityDbContext<Account, Role, int, AccountLogin, AccountRole, AccountClaim>
    {
        /// <summary>
        ///  Represents the clients in the database.
        /// </summary>
        public DbSet<Client> Clients { get; set; }

        /// <summary>
        ///  Represents the refresh tokens in the database.
        /// </summary>
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        /// <summary>
        ///  Standard constructor.
        /// </summary>
        public ApiContext() : base("ApiContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApiContext, Configuration>());
        }

        /// <summary>
        ///  Creates a new ApiContext object.
        /// </summary>
        /// <returns>An ApiContext object.</returns>
        public static ApiContext Create()
        {
            return new ApiContext();
        }

        /// <summary>
        ///  Configures the model of the ApiContext before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Account>()
                .ToTable("Account")
                .HasKey(r => r.Id);

            modelBuilder.Entity<AccountRole>()
                .ToTable("AccountRole")
                .HasKey(r => new { r.RoleId, r.UserId })
                .Property(au => au.UserId).HasColumnName("AccountId");

            modelBuilder.Entity<AccountLogin>()
                .ToTable("AccountLogin")
                .Property(au => au.UserId).HasColumnName("AccountId");

            modelBuilder.Entity<AccountClaim>()
                .ToTable("AccountClaim")
                .Property(au => au.UserId).HasColumnName("AccountId");

            modelBuilder.Entity<Role>()
                .ToTable("Role")
                .HasKey(r => r.Id);
        }
    }
}