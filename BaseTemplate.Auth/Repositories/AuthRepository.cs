﻿using System;
using System.Linq;
using System.Threading.Tasks;
using BaseTemplate.Entities.DataEntities;
using Microsoft.Owin;

namespace BaseTemplate.Auth.Repositories
{
    /// <summary>
    ///  Custom class to support the ASP.NET Identity system.
    /// </summary>
    public class AuthRepository : IDisposable
    {
        /// <summary>
        ///  Our EntityFramework context.
        /// </summary>
        private readonly ApiContext _ctx;

        /// <summary>
        ///  Standard constructor.
        /// </summary>
        public AuthRepository(IOwinContext context)
        {
            _ctx = new ApiContext();
        }

        /// <summary>
        ///  To dispose of the EntityFramework context.
        /// </summary>
        public void Dispose()
        {
            _ctx.Dispose();
        }

        /// <summary>
        ///  Adds a refresh token to the database.
        /// </summary>
        /// <param name="token">Token to add.</param>
        /// <returns>Task to enable asynchronous execution.</returns>
        public async Task<bool> AddRefreshToken(RefreshToken token)
        {
            RefreshToken existingToken = _ctx.RefreshTokens.SingleOrDefault(r => r.Subject == token.Subject && r.ClientId == token.ClientId);

            if (existingToken != null)
            {
                bool result = await RemoveRefreshToken(existingToken);
            }

            _ctx.RefreshTokens.Add(token);

            return await _ctx.SaveChangesAsync() > 0;
        }

        /// <summary>
        ///  Finds a client in the database.
        /// </summary>
        /// <param name="clientId">ID of the client we want to find.</param>
        /// <returns>A client object.</returns>
        public Client FindClient(string clientId)
        {
            Client client = _ctx.Clients.Find(clientId);

            return client;
        }

        /// <summary>
        ///  Finds a refresh token in the database.
        /// </summary>
        /// <param name="refreshTokenId"></param>
        /// <returns>A refresh token.</returns>
        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            RefreshToken refreshToken = await _ctx.RefreshTokens.FindAsync(refreshTokenId);

            return refreshToken;
        }

        /// <summary>
        ///  Removes a refresh token from the database.
        /// </summary>
        /// <param name="refreshTokenId">ID or the refresh token to remove.</param>
        /// <returns>A boolean of whether or not the removal was a success.</returns>
        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
            RefreshToken refreshToken = await _ctx.RefreshTokens.FindAsync(refreshTokenId);

            if (refreshToken != null)
            {
                _ctx.RefreshTokens.Remove(refreshToken);
                return await _ctx.SaveChangesAsync() > 0;
            }

            return false;
        }

        /// <summary>
        ///  Removes a refresh token from the database.
        /// </summary>
        /// <param name="refreshToken">Refresh token to remove.</param>
        /// <returns>A boolean of whether or not the removal was a success.</returns>
        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            _ctx.RefreshTokens.Remove(refreshToken);
            return await _ctx.SaveChangesAsync() > 0;
        }
    }
}