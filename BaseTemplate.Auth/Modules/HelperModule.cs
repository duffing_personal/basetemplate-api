﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace BaseTemplate.Auth.Modules
{
    /// <summary>
    ///  Contains hodge podge of random help methods used throughout the application that don't belong anywhere else.
    /// </summary>
    public class HelperModule
    {
        /// <summary>
        ///  Hashes a piece of text along with a salt.
        /// </summary>
        /// <param name="text">Text we want to hash.</param>
        /// <param name="salt">Salt to append to text before hashing takes place. This adds additional security.</param>
        /// <returns>The hashed piece of text.</returns>
        public static string GetHash(string text, string salt = null)
        {
            SHA512Managed hashString = new SHA512Managed();

            string textToHash;

            if (salt != null)
            {
                textToHash = text;
            }
            else
            {
                textToHash = string.Concat(text, salt);
            }

            byte[] textWithSaltBytes = Encoding.UTF8.GetBytes(textToHash);
            byte[] hashedBytes = hashString.ComputeHash(textWithSaltBytes);

            hashString.Clear();

            return Convert.ToBase64String(hashedBytes);
        }

        /// <summary>
        ///  Creates a secret (base 64 encoded string) for a client app. Used for the encryption and decryption of JSON web tokens.
        /// </summary>
        /// <returns>The base 64 encoded string.</returns>
        public static string GetSecret()
        {
            byte[] key = new byte[32];
            RNGCryptoServiceProvider.Create().GetBytes(key);
            string base64Secret = Convert.ToBase64String(key);

            return base64Secret;
        }
    }
}