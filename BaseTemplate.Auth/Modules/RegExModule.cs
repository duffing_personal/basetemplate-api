﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace BaseTemplate.Auth.Modules
{
    /// <summary>
    ///  Contains helper methods used for validating strings using regular expressions.
    /// </summary>
    public class RegExModule
    {
        /// <summary>
        ///  Global boolean for determining string is valid or not.
        /// </summary>
        private bool invalid;

        /// <summary>
        ///  Determines if an email is in a valid format.
        /// </summary>
        /// <param name="strIn">Email to be validated.</param>
        /// <returns>Boolean if valid or not.</returns>
        public bool IsValidEmail(string strIn)
        {
            invalid = false;
            if (String.IsNullOrEmpty(strIn))
            {
                return false;
            }

            /* Use IdnMapping class to convert Unicode domain names. */
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
            {
                return false;
            }

            /* Return true if strIn is in valid e-mail format. */
            try
            {
                return Regex.IsMatch(strIn,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="match"></param>
        /// <returns></returns>
        private string DomainMapper(Match match)
        {
            /* IdnMapping class with default property values. */
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }
    }
}