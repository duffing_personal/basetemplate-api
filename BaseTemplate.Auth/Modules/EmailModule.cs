﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace BaseTemplate.Auth.Modules
{
    /// <summary>
    ///  Email service used by the ApplicationUserMAnager.
    /// </summary>
    public class EmailModule : IIdentityMessageService
    {
        /// <summary>
        ///  Sends the email to a user.
        /// </summary>
        /// <param name="message">The message contained in the email.</param>
        public async Task SendAsync(IdentityMessage message)
        {
            /* Credentials: */
            string smtpServer = Properties.Settings.Default.SmtpServer;
            string smtpUsername = Properties.Settings.Default.SmtpUsername;
            string smtpPassword = Properties.Settings.Default.SmtpPassword;
            int smtpPort = Properties.Settings.Default.SmtpPort;
            bool enableSsl = true;
            string sentFrom = Properties.Settings.Default.Email;

            /* Configure the client: */
            SmtpClient client = new SmtpClient(smtpServer, Convert.ToInt32(587));

            client.Port = smtpPort;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.EnableSsl = enableSsl;

            /* Create the credentials: */
            NetworkCredential credentials = new NetworkCredential(smtpUsername, smtpPassword);
            client.Credentials = credentials;

            /* Create the message: */
            MailMessage mail = new MailMessage(sentFrom, message.Destination);

            mail.Subject = message.Subject;
            mail.Body = message.Body;

            /* Send: */
            await client.SendMailAsync(mail);
        }
    }
}
