﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using BaseTemplate.Auth.Identity;
using BaseTemplate.Auth.Models;
using BaseTemplate.Auth.Results;
using BaseTemplate.Entities.IdentityEntities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace BaseTemplate.Auth.Controllers
{
    /// <summary>
    ///  WebAPI controller that handles additional management tasks for user accounts for the application.
    /// </summary>
    [RoutePrefix("Manage")]
    [Authorize]
    public class ManageController : ApiController
    {
        /* Used for XSRF protection when adding external logins */
        private const string XsrfKey = "XsrfId";

        /// <summary>
        ///  Interacts with the Owin authentication middleware.
        /// </summary>
        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        /// <summary>
        ///  Used for finding users, adding users, modifying users, etc. through the standard ASP.NET Identity implementation.
        /// </summary>
        private UserManager<Account, int> _userManager
        {
            get { return Request.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        }

        /// <summary>
        ///  Changes a user's password.
        /// </summary>
        /// <param name="currentPassword">Current password for the user.</param>
        /// <param name="newPassword">Desired password for the user.</param>
        /// <returns>An IHttpActionResult.</returns>
        public async Task<IHttpActionResult> ChangePassword(string currentPassword, string newPassword)
        {
            Account user = await _userManager.FindByNameAsync(User.Identity.Name);

            IdentityResult result = await _userManager.ChangePasswordAsync(user.Id, currentPassword, newPassword);

            if (!result.Succeeded)
            {
                return BadRequest("Password wasn't changed!");
            }

            return Ok();
        }

        /// <summary>
        ///  Handles creating the challenge result for when a user wants to link additional external accounts to their local account.
        /// </summary>
        /// <param name="userName">Username of the user we're trying to link another account to.</param>
        /// <param name="provider">External provider the user is trying to link an account from.</param>
        /// <param name="hashedSecurityToken">Security token to validate that this user is who they say they are.</param>
        /// <returns>An IHttpActionResult</returns>
        [Route("LinkLogin", Name = "LinkLogin")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetLinkLogin(string userName, string provider, string hashedSecurityToken)
        {
            Account user = await _userManager.FindByNameAsync(userName);

            if (user.HashedSecurityToken != hashedSecurityToken)
            {
                return BadRequest("Error happened linking accounts!");
            }

            string redirectUri = "Manage/LinkLoginCallback?userId=" + user.Id + "&hashedSecurityToken=" + HttpUtility.UrlEncode(hashedSecurityToken);

            return new ChallengeResult(provider, this, redirectUri, user.Id);
        }

        /// <summary>
        ///  Finalizes the link between a local account and an external provider.
        /// </summary>
        /// <param name="userId">ID of the user trying to link accounts.</param>
        /// <param name="hashedSecurityToken">Security token to validate the user is who they say they are before accounts are linked together.</param>
        /// <returns>An IHttpActionResult.</returns>
        [Route("LinkLoginCallback", Name = "LinkLoginCallback")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetLinkLoginCallback(string userId, string hashedSecurityToken)
        {
            Account user = await _userManager.FindByIdAsync(Convert.ToInt32(userId));

            ExternalLoginInfo loginInfo = await Authentication.GetExternalLoginInfoAsync(XsrfKey, userId);

            if (loginInfo == null || (user.HashedSecurityToken != hashedSecurityToken))
            {
                return BadRequest("Error happened linking accounts!");
            }

            IdentityResult result = await _userManager.AddLoginAsync(Convert.ToInt32(userId), loginInfo.Login);

            if (!result.Succeeded)
            {
                return BadRequest(result.Errors.ElementAt(0));
            }

            /* Redirects the child window popup to a new html page for executing a script that ends up calling our main clientside code. */
            return Redirect(Properties.Settings.Default.SiteURL + "/connectcomplete.html");
        }

        /// <summary>
        ///  Retrieve all of the logins that an account has associated with them (including external provider logins).
        /// </summary>
        /// <returns>List of logins.</returns>
        public async Task<List<object>> GetLogins()
        {
            Account user = await _userManager.FindByNameAsync(User.Identity.Name);

            IList<UserLoginInfo> userLogins = await _userManager.GetLoginsAsync(user.Id);

            List<AuthenticationDescription> otherLogins = Authentication.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();

            List<object> loginList = new List<object>();

            loginList.Add(userLogins);
            loginList.Add(otherLogins);

            return loginList;
        }

        /// <summary>
        ///  Get all of the info for the user for managing their account.
        /// </summary>
        /// <returns>An IHttpActionREsult containing the info.</returns>
        [Route("ManageInfo", Name = "ManageInfo")]
        public async Task<IHttpActionResult> GetManageInfo()
        {
            Account user = await _userManager.FindByNameAsync(User.Identity.Name);

            var manageInfo = new
            {
                logins = await GetLogins(),
                hasPassword = user.PasswordHash != null,
                hashedSecurityToken = HttpUtility.UrlEncode(user.HashedSecurityToken)
            };

            return Ok(manageInfo);
        }

        /// <summary>
        ///  Creates a password for a user if they do not already have one.
        /// </summary>
        /// <param name="password">CreatePasswordModel that's created by clientside code.</param>
        /// <returns>An IHttpActionResult.</returns>
        [HttpPost]
        [Route("CreatePassword", Name = "CreatePassword")]
        public async Task<IHttpActionResult> PostCreatePassword(CreatePasswordModel password)
        {
            Account user = await _userManager.FindByNameAsync(User.Identity.Name);

            IdentityResult result = await _userManager.AddPasswordAsync(user.Id, password.password);

            if (!result.Succeeded)
            {
                return BadRequest("Password wasn't created!");
            }

            return Ok();
        }

        /// <summary>
        ///  Unlinks an external provider login from an account.
        /// </summary>
        /// <param name="login">The external login to unlink from the account.</param>
        /// <returns>An IHttpActionResult.</returns>
        [HttpPost]
        [Route("RemoveLogin", Name = "RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(UserLoginInfo login)
        {
            Account user = await _userManager.FindByNameAsync(User.Identity.Name);

            bool hasPassword = user.PasswordHash != null;

            IList<UserLoginInfo> userLogins = await _userManager.GetLoginsAsync(user.Id);

            /* The login can only be removed if this isn't the only social networking login or a password has been established */
            if (userLogins.Count() > 1 || hasPassword)
            {
                IdentityResult result = await _userManager.RemoveLoginAsync(user.Id, login);

                if (!result.Succeeded)
                {
                    return BadRequest("Error unlinking account");
                }
            }

            return Ok();
        }
    }
}