﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BaseTemplate.Auth.Identity;
using BaseTemplate.Auth.Models;
using BaseTemplate.Auth.Modules;
using BaseTemplate.Auth.Repositories;
using BaseTemplate.Auth.Results;
using BaseTemplate.Entities.DataEntities;
using BaseTemplate.Entities.IdentityEntities;

namespace BaseTemplate.Auth.Controllers
{
    /// <summary>
    ///  WebAPI controller that handles authorization for the application.
    /// </summary>
    [RoutePrefix("Authorization")]
    public class AuthorizationController : ApiController
    {
        /// <summary>
        ///  Helper for validating strings using regular expressions.
        /// </summary>
        private readonly RegExModule _regexUtility;

        /// <summary>
        ///  Interacts with the Owin authentication middleware.
        /// </summary>
        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        /// <summary>
        ///  Used for finding users, adding users, modifying users, etc. through the standard ASP.NET Identity implementation.
        /// </summary>
        private UserManager<Account, int> _userManager
        {
            get { return Request.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        }
        
        /// <summary>
        ///  Used for accessing refresh tokens and clients in the application database.
        /// </summary>
        private AuthRepository _repo
        {
            get { return new AuthRepository(Request.GetOwinContext()); }
        }

        /// <summary>
        ///  Standard constructor for the WebAPI controller.
        /// </summary>
        public AuthorizationController()
        {
            _regexUtility = new RegExModule();
        }

        /// <summary>
        ///  Takes in a user's ID and an email code and confirms that user's email if the email code is valid.
        /// </summary>
        /// <param name="userId">Primary key of the user to confirm an email for.</param>
        /// <param name="code">Email validation code used for confirmation.</param>
        /// <returns>An IHttpActionResult.</returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("ConfirmEmail")]
        public async Task<IHttpActionResult> ConfirmEmail(int userId, string code)
        {
            IdentityResult result;

            if (userId == 0 || code == null)
            {
                return BadRequest("Both the token and user id need to be populated");
            }

            try
            {
                result = await _userManager.ConfirmEmailAsync(userId, code);
            }
            catch (InvalidOperationException ex)
            {
                // ConfirmEmailAsync throws when the userId is not found.
                return BadRequest(ex.Message);
            }

            if (result.Succeeded)
            {
                return Redirect(Properties.Settings.Default.SiteURL + "/#/login");
            }

            // if we get this far something has gone wrong.
            return BadRequest("email was not confirmed successfully.");
        }

        /// <summary>
        ///  Retrieves the GET requests originating from the application for logging in using OAuth.
        /// </summary>
        /// <param name="provider">OAuth provider being used for logging into the application.</param>
        /// <param name="error">Error message to display to the user.</param>
        /// <returns>An IHttpActionResult.</returns>
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("RetrieveLoginExternal")]
        public async Task<IHttpActionResult> GetLoginExternal(string provider, string error = null)
        {
            string redirectUri = string.Empty;

            /* If we have passed in an error let's shoot it back out to the user. */
            if (error != null)
            {
                return BadRequest(Uri.EscapeDataString(error));
            }

            /* If the user doesn't have an external cookie yet from an external provider then we need to issue the challenge result to get this info. */
            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            string redirectUriValidationResult = ValidateClientAndRedirectUri(Request, ref redirectUri);

            if (!string.IsNullOrWhiteSpace(redirectUriValidationResult))
            {
                return BadRequest(redirectUriValidationResult);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            Account user = await _userManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider, externalLogin.ProviderKey));

            bool hasRegistered = user != null;

            redirectUri = string.Format("{0}#external_access_token={1}&provider={2}&haslocalaccount={3}&external_user_name={4}&external_email={5}",
                    redirectUri,
                    externalLogin.ExternalAccessToken,
                    externalLogin.LoginProvider,
                    hasRegistered,
                    externalLogin.UserName,
                    externalLogin.Email);

            return Redirect(redirectUri);
        }

        /// <summary>
        ///  Generates local access tokens for external users who already registered with a local account and linked their external identities to a local account.
        /// </summary>
        /// <param name="provider">OAuth provider being used to log into the application.</param>
        /// <param name="externalAccessToken">Token issued by the external provider.</param>
        /// <returns>An IHttpActionResult.</returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("ObtainLocalAccessToken")]
        public async Task<IHttpActionResult> ObtainLocalAccessToken(string provider, string externalAccessToken)
        {
            if (string.IsNullOrWhiteSpace(provider) || string.IsNullOrWhiteSpace(externalAccessToken))
            {
                return BadRequest("Provider or external access token is not sent");
            }

            ParsedExternalAccessTokenModel verifiedAccessToken = await VerifyExternalAccessToken(provider, externalAccessToken);

            if (verifiedAccessToken == null)
            {
                return BadRequest("Invalid Provider or External Access Token");
            }

            Account user = await _userManager.FindAsync(new UserLoginInfo(provider, verifiedAccessToken.user_id));

            bool hasRegistered = user != null;

            if (!hasRegistered)
            {
                return BadRequest("External user is not registered");
            }

            /* generate access token response */
            JObject accessTokenResponse = GenerateLocalAccessTokenResponse(user.UserName, user.Email);

            return Ok(accessTokenResponse);
        }

        /// <summary>
        ///  Used for sending the password reset email to a user.
        /// </summary>
        /// <param name="userName">Username of the user we're sending an email to.</param>
        /// <returns>An IHttpActionResult.</returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("PasswordResetEmail")]
        public async Task<IHttpActionResult> PasswordResetEmail(string userName)
        {
            if (userName == null)
            {
                ModelState.AddModelError("", "You need to enter a username!");

                return BadRequest(ModelState);
            }

            Account user = await _userManager.FindByNameAsync(userName);

            /* If a user's email isn't confirmed we can't send a password reset email to them. */
            if (user != null && !user.EmailConfirmed)
            {
                /* We need to expose all of the headers we add below to the client application. */
                HttpContext.Current.Response.Headers.Add("Access-Control-Expose-Headers", "callbackData, callbackText, callbackMethod, callbackService, callbackMessage");

                /* All of these headers are used by the client application for displaying an error message to the user. */
                HttpContext.Current.Response.Headers.Add("callbackMethod", "sendConfirmationEmail");
                HttpContext.Current.Response.Headers.Add("callbackService", "authService");
                HttpContext.Current.Response.Headers.Add("callbackData", user.UserName);
                HttpContext.Current.Response.Headers.Add("callbackText", "Resend confirmation email");
                HttpContext.Current.Response.Headers.Add("callbackMessage", "Email isn't confirmed.");

                /* Error message added to the ModelState for good measure. */
                ModelState.AddModelError("", "Your email isn't confirmed yet!");

                return BadRequest(ModelState);               
            }

            if (user != null)
            {
                /* Construct the url that will be sent to the user's email for resetting the password. */
                string token = HttpUtility.UrlEncode(await _userManager.GeneratePasswordResetTokenAsync(user.Id));
                string stringUrl = String.Format(Properties.Settings.Default.SiteURL + "/#/resetPassword?userId={0}&token={1}", user.Id, token);

                await _userManager.SendEmailAsync(user.Id, "Reset your password", "Please click the following link to reset your password: " + stringUrl);
            }
            else
            {
                ModelState.AddModelError("", "That user doesn't exist in our system!");

                return BadRequest(ModelState);
            }

            return Ok();
        }

        /// <summary>
        ///  Creates a user for the application.
        /// </summary>
        /// <param name="userModel">User information passed in from the sign-up form clientside.</param>
        /// <returns>An IHttpActionResult.</returns>
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(UserModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            /* validate that username is a valid email (as best we can, it's up to the user for the most part) */
            if (!_regexUtility.IsValidEmail(userModel.Email))
            {
                ModelState.AddModelError("", "invalid email address.");

                return BadRequest(ModelState);
            }

            /* We hash some text for the user's security token that's associated with their account. */
            string textToHash = String.Format("{0}{1}{2}", DateTime.Now, userModel.UserName, userModel.Password);

            /* Construct the user object that we're going to try and create in the database. */
            Account createUser = new Account
            {
                UserName = userModel.UserName,
                Email = userModel.Email,
                HashedSecurityToken = HelperModule.GetHash(textToHash, userModel.Email)
            };

            IdentityResult result = await _userManager.CreateAsync(createUser, userModel.Password);

            if (result == null || !result.Succeeded)
            {
                IHttpActionResult errorResult = GetErrorResult(result);

                if (errorResult != null)
                {
                    return errorResult;
                }
            }
            else
            {
                /* We need to get the newly create user account for the user's ID. */
                Account user = await _userManager.FindByNameAsync(userModel.UserName);

                /* Create url to send to user for confirming their email in order to log into the application. */
                string code = HttpUtility.UrlEncode(await _userManager.GenerateEmailConfirmationTokenAsync(user.Id));
                string stringUrl = String.Format("ConfirmEmail?userId={0}&code={1}", user.Id, code);
                string callbackUrl = Url.Content(stringUrl);

                await _userManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking this link: " + callbackUrl);

                return Ok();
            }

            /* if we get here there was an issue somewhere! */
            ModelState.AddModelError("", "error occurred in registration process, please try again later.");

            return BadRequest(ModelState);
        }

        /// <summary>
        ///  Used to add an external user as a local account, and then link it with the created local account.
        /// </summary>
        /// <param name="model">RegisterExternalBindingModel object that's auto created before this method is called.</param>
        /// <returns>An IHttpActionResult.</returns>
        [AllowAnonymous]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ParsedExternalAccessTokenModel verifiedAccessToken = await VerifyExternalAccessToken(model.Provider, model.ExternalAccessToken);

            if (verifiedAccessToken == null)
            {
                return BadRequest("Invalid Provider or External Access Token");
            }

            /* This should only be null if they're trying to register from Facebook and didn't enter an email. */
            if (model.Email == null)
            {
                return BadRequest("You need to enter an email before continuning.");
            }

            /* Validate that username is a valid email (as best we can, its up to the user for the most part). */
            if (!_regexUtility.IsValidEmail(model.Email))
            {
                return BadRequest("Invalid email address.");
            }

            /* Let's check if this user is already registered. */
            Account user = await _userManager.FindAsync(new UserLoginInfo(model.Provider, verifiedAccessToken.user_id));

            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                return BadRequest("External user is already registered");
            }

            /* Create the user's hashed security token. */
            string textToHash = String.Format("{0}{1}{2}", DateTime.Now, model.UserName, model.Email);

            user = new Account
            {
                UserName = model.UserName,
                Email = model.Email,
                HashedSecurityToken = HelperModule.GetHash(textToHash, model.Email),
                EmailConfirmed = model.Provider == "Google" /* When registering from an oauth service we'll assume that the email is confirmed only if from google */
            };

            IdentityResult result = await _userManager.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            ExternalLoginInfo info = new ExternalLoginInfo
            {
                DefaultUserName = model.UserName,
                Login = new UserLoginInfo(model.Provider, verifiedAccessToken.user_id)
            };

            /* Add this newly created local user to the database. */
            result = await _userManager.AddLoginAsync(user.Id, info.Login);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            /* Generate access token response */
            JObject accessTokenResponse = GenerateLocalAccessTokenResponse(model.UserName, model.Email);

            /* If the provider is Facebook the email wasn't sent by them through oauth, and is thus unconfirmed. we can't let the user login until it's confirmed */
            if (model.Provider == "Facebook")
            {
                /* Generate email token */
                string code = HttpUtility.UrlEncode(await _userManager.GenerateEmailConfirmationTokenAsync(user.Id));
                string stringUrl = String.Format("ConfirmEmail?userId={0}&code={1}", user.Id, code);
                string callbackUrl = Url.Content(stringUrl);

                await _userManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking this link: " + callbackUrl);

                return Ok();
            }

            return Ok(accessTokenResponse);
        }

        /// <summary>
        ///  Resets a user's password.
        /// </summary>
        /// <param name="resetModel">ResetPasswordModel sent from clientside.</param>
        /// <returns>An IHttpActionResult.</returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("ResetPassword")]
        public async Task<IHttpActionResult> ResetPassword(ResetPasswordModel resetModel)
        {
            IdentityResult result;

            if (resetModel.userId == 0 || resetModel.token == null || resetModel.password.Length == 0)
            {
                ModelState.AddModelError("", "Something went wrong during the password reset.");

                return BadRequest(ModelState);
            }

            if (resetModel.password != resetModel.confirmPassword)
            {
                ModelState.AddModelError("", "Passwords don't match.");

                return BadRequest(ModelState);
            }
            
            try
            {
                result = await _userManager.ResetPasswordAsync(resetModel.userId, resetModel.token, resetModel.password);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Something went wrong during the password reset.");

                return BadRequest(ModelState);
            }

            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return GetErrorResult(result);
            }
        }

        /// <summary>
        ///  Sends an email to a user asking to confirm their email.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("SendConfirmationEmail")]
        [HttpGet]
        public async Task<IHttpActionResult> SendConfirmationEmail(string userName)
        {
            Account user = _userManager.FindByName(userName);

            /* Generate email token */
            string code = HttpUtility.UrlEncode(await _userManager.GenerateEmailConfirmationTokenAsync(user.Id));
            string stringUrl = String.Format("ConfirmEmail?userId={0}&code={1}", user.Id, code);
            string callbackUrl = Url.Content(stringUrl);

            await _userManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking this link: " + callbackUrl);

            return Ok();
        }

        /// <summary>
        ///  Issues local access token which can be used to access secure back-end API end points.
        /// </summary>
        /// <param name="userName">Username of the user we're issuing a local access token for.</param>
        /// <param name="email">Email of the user we're issuing a local access token for.</param>
        /// <returns>A JObject.</returns>
        private JObject GenerateLocalAccessTokenResponse(string userName, string email)
        {
            TimeSpan tokenExpiration = TimeSpan.FromDays(1);

            ClaimsIdentity identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);

            identity.AddClaim(new Claim(ClaimTypes.Name, userName));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, email));

            AuthenticationProperties props = new AuthenticationProperties
            {
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.Add(tokenExpiration)
            };

            props.Dictionary.Add("as:client_id", "ngAuthApp");

            AuthenticationTicket ticket = new AuthenticationTicket(identity, props);
            string accessToken;
            try
            {
                accessToken = Startup.OAuthBearerOptions.AccessTokenFormat.Protect(ticket);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            JObject tokenResponse = new JObject(
                new JProperty("userName", userName),
                new JProperty("access_token", accessToken),
                new JProperty("token_type", "bearer"),
                new JProperty("expires_in", tokenExpiration.TotalSeconds.ToString()),
                new JProperty(".issued", ticket.Properties.IssuedUtc.ToString()),
                new JProperty(".expires", ticket.Properties.ExpiresUtc.ToString())
            );

            return tokenResponse;
        }

        /// <summary>
        ///  A generic method for creating a concatenated error message for the user.
        /// </summary>
        /// <param name="result">IdentityResult that contains errors.</param>
        /// <returns>An IHttpActionResult.</returns>
        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        /// <summary>
        ///  Retrieves a query string variable from a request url.
        /// </summary>
        /// <param name="request">Request to grab the query string variable from.</param>
        /// <param name="key">The key of the query string variable.</param>
        /// <returns>The value of the query string variable.</returns>
        private string GetQueryString(HttpRequestMessage request, string key)
        {
            IEnumerable<KeyValuePair<string, string>> queryStrings = request.GetQueryNameValuePairs();

            if (queryStrings == null)
            {
                return null;
            }

            KeyValuePair<string, string> match = queryStrings.FirstOrDefault(keyValue => String.Compare(keyValue.Key, key, StringComparison.OrdinalIgnoreCase) == 0);

            if (string.IsNullOrEmpty(match.Value))
            {
                return null;
            }

            return match.Value;
        }

        /// <summary>
        ///  Validates that client and redirect URI which is set by the application is valid and that the requesting client is configures to allow redirect for this URI.
        /// </summary>
        /// <param name="request">The user's request.</param>
        /// <param name="redirectUriOutput">URI for output.</param>
        /// <returns>An error message or empty if valid.</returns>
        private string ValidateClientAndRedirectUri(HttpRequestMessage request, ref string redirectUriOutput)
        {
            Uri redirectUri;

            string redirectUriString = GetQueryString(Request, "redirect_uri");

            if (string.IsNullOrWhiteSpace(redirectUriString))
            {
                return "redirect_uri is required";
            }

            bool validUri = Uri.TryCreate(redirectUriString, UriKind.Absolute, out redirectUri);

            if (!validUri)
            {
                return "redirect_uri is invalid";
            }

            string clientId = GetQueryString(Request, "client_id");

            if (string.IsNullOrWhiteSpace(clientId))
            {
                return "client_Id is required";
            }

            Client client = _repo.FindClient(clientId);

            if (client == null)
            {
                return string.Format("Client_id '{0}' is not registered in the system.", clientId);
            }

            if (
                !string.Equals(client.AllowedOrigin, redirectUri.GetLeftPart(UriPartial.Authority),
                    StringComparison.OrdinalIgnoreCase))
            {
                return string.Format("The given URL is not allowed by Client_id '{0}' configuration.", clientId);
            }

            redirectUriOutput = redirectUri.AbsoluteUri;

            return string.Empty;
        }

        /// <summary>
        ///  Confirms that an external access token that's sent from the clientside to the API is valid (not expired and issued to the same client that's configured in the API)
        /// </summary>
        /// <param name="provider">External provider that issued the access token.</param>
        /// <param name="accessToken">Access token we're validating.</param>
        /// <returns></returns>
        private async Task<ParsedExternalAccessTokenModel> VerifyExternalAccessToken(string provider, string accessToken)
        {
            ParsedExternalAccessTokenModel parsedToken = null;

            string verifyTokenEndPoint = "";

            if (provider == "Facebook")
            {
                string appToken = Properties.Settings.Default.FacebookAppToken;
                verifyTokenEndPoint = string.Format("https://graph.facebook.com/debug_token?input_token={0}&access_token={1}", accessToken, appToken);
            }
            else if (provider == "Google")
            {
                verifyTokenEndPoint = string.Format("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token={0}", accessToken);
            }
            else
            {
                return null;
            }

            HttpClient client = new HttpClient();
            Uri uri = new Uri(verifyTokenEndPoint);
            HttpResponseMessage response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();

                dynamic jObj = (JObject) JsonConvert.DeserializeObject(content);

                parsedToken = new ParsedExternalAccessTokenModel();

                if (provider == "Facebook")
                {
                    parsedToken.user_id = jObj["data"]["user_id"];
                    parsedToken.app_id = jObj["data"]["app_id"];

                    if (!string.Equals(Startup.FacebookAuthOptions.AppId, parsedToken.app_id, StringComparison.OrdinalIgnoreCase))
                    {
                        return null;
                    }
                }
                else if (provider == "Google")
                {
                    parsedToken.user_id = jObj["user_id"];
                    parsedToken.app_id = jObj["audience"];

                    if (!string.Equals(Startup.GoogleAuthOptions.ClientId, parsedToken.app_id, StringComparison.OrdinalIgnoreCase))
                    {
                        return null;
                    }
                }
            }

            return parsedToken;
        }
    }
}