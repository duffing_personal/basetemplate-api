﻿using System;
using System.Web.Http;
using BaseTemplate.Auth;
using BaseTemplate.Auth.Identity;
using BaseTemplate.Auth.Providers;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Owin;

[assembly: OwinStartup(typeof (Startup))]

namespace BaseTemplate.Auth
{
    /// <summary>
    ///  Standard Owin Startup class.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        public static FacebookAuthenticationOptions FacebookAuthOptions { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public static GoogleOAuth2AuthenticationOptions GoogleAuthOptions { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }

        /// <summary>
        ///  Configures the settings for the application.
        /// </summary>
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            ConfigureOAuth(app);

            WebApiConfig.Register(config);
            
            app.UseWebApi(config);
        }

        /// <summary>
        ///  Configures the OAuth settings for the application.
        /// </summary>
        public void ConfigureOAuth(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);

            /* Configure the db context and user manager to use a single instance per request */
            app.CreatePerOwinContext(ApiContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            /* Use a cookie to temporarily store information about a user logging in with a third party login provider */
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);

            OAuthAuthorizationServerOptions oAuthServerOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = false,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(30),
                AuthorizeEndpointPath = new PathString("/auth/Authorization/ExternalLogin"),
                Provider = new SimpleAuthorizationServerProvider(),
                RefreshTokenProvider = new SimpleRefreshTokenProvider(),
                AccessTokenFormat = new SimpleJwtProvider(Properties.Settings.Default.SiteURL)
            };

            /* WebApi controllers with an [Authorize] attribute will be validated with JWT */
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] { Properties.Settings.Default.ClientID },
                    IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new SymmetricKeyIssuerSecurityTokenProvider(Properties.Settings.Default.SiteURL, Properties.Settings.Default.ClientSecret)
                    }
                });

            /* Token Generation */
            app.UseOAuthAuthorizationServer(oAuthServerOptions);

            /* Configure Google External Login */
            GoogleAuthOptions = new GoogleOAuth2AuthenticationOptions
            {
                ClientId = Properties.Settings.Default.GoogleClientID,
                ClientSecret = Properties.Settings.Default.GoogleClientSecret,
                Provider = new GoogleAuthProvider()
            };
            app.UseGoogleAuthentication(GoogleAuthOptions);

            /* Configure Facebook External Login */
            FacebookAuthOptions = new FacebookAuthenticationOptions
            {
                AppId = Properties.Settings.Default.FacebookAppID,
                AppSecret = Properties.Settings.Default.FacebookAppSecret,
                Provider = new FacebookAuthProvider()
            };
            app.UseFacebookAuthentication(FacebookAuthOptions);
        }
    }
}