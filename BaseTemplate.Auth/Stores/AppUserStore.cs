﻿using BaseTemplate.Entities.IdentityEntities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BaseTemplate.Auth.Stores
{
    /// <summary>
    ///  Overriden class representing the Entity Framework implementation of a user store.
    /// </summary>
    public class AppUserStore : UserStore<Account, Role, int, AccountLogin, AccountRole, AccountClaim>
    {
        /// <summary>
        ///  Standard constructor.
        /// </summary>
        /// <param name="context">Entity Framework context object.</param>
        public AppUserStore(ApiContext context)
            : base(context)
        {
        }
    }
}