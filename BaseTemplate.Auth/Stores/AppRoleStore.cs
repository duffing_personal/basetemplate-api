﻿using BaseTemplate.Entities.IdentityEntities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BaseTemplate.Auth.Stores
{
    /// <summary>
    ///  Overriden class representing the Entity Framework implementation of a role store.
    /// </summary>
    public class AppRoleStore : RoleStore<Role, int, AccountRole>
    {
        /// <summary>
        ///  Standard constructor.
        /// </summary>
        /// <param name="context">Entity Framework context object.</param>
        public AppRoleStore(ApiContext context)
            : base(context)
        {
        }
    }
}