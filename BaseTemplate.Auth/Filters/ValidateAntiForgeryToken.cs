﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using BaseTemplate.Auth.Modules;

namespace BaseTemplate.Auth.Filters
{
    /// <summary>
    ///  Global filter that's used for protecting against CSRF attacks.
    /// </summary>
    public class ValidateAntiForgeryToken : ActionFilterAttribute
    {
        /// <summary>
        ///  Expected name of the cookie we're looking for.
        /// </summary>
        private const string XsrfCookieName = "XSRF-TOKEN";

        /// <summary>
        ///  Expecting name of the header we're looking for.
        /// </summary>
        private const string XsrfHeaderName = "X-XSRF-TOKEN";

        /// <summary>
        ///  Salt used for csrf token creation.
        /// </summary>
        private const string CsrfTokenSalt = "veZz$OaHqiENX54-KTF8m2#*&i";

        /// <summary>
        ///  Paths we ignore for csrf attacks.
        /// </summary>
        private readonly string[] AllowedPaths = { "/auth/Authorization/registerexternal", "/auth/Authorization/register", "/auth/Authorization/ResetPassword" };

        /// <summary>
        ///  This runs on every request to any endpoint on the Auth and/or Api layers.
        /// </summary>
        /// <param name="filterContext">HttpActionContext object for the request.</param>
        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            string requestMethod = filterContext.Request.Method.Method;
            string requestPath = filterContext.Request.RequestUri.AbsolutePath;
            string host = filterContext.Request.RequestUri.Host;
            Boolean isValid = true;

            /* CSRF attacks are ignored on GET request and certain POST requests as well. CSRF headers and cookies don't work on localhost development. */
            if (requestMethod != "GET" && !(AllowedPaths.Contains(requestPath) && requestMethod == "POST") && host != "localhost")
            {
                var headerToken = filterContext.Request.Headers.Where(x => x.Key.Equals(XsrfHeaderName, StringComparison.OrdinalIgnoreCase))
                    .Select(x => x.Value).SelectMany(x => x).FirstOrDefault();

                var cookieToken = filterContext.Request.Headers.GetCookies().Select(x => x[XsrfCookieName]).FirstOrDefault();

                /* Check for missing cookie or header */
                if (cookieToken == null || headerToken == null)
                {
                    isValid = false;
                }

                /* Ensure that the cookie matches the header */
                if (isValid && !String.Equals(headerToken, cookieToken.Value, StringComparison.OrdinalIgnoreCase))
                {
                    isValid = false;
                }

                if (!isValid)
                {
                    filterContext.Response = filterContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                    filterContext.Response.ReasonPhrase = "Unauthorized to make that request.";
                    return;
                }
            }

            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        ///  This executes after an action has taken place against an endpoint on the Auth and/or Api layers.
        /// </summary>
        /// <param name="actionExecutedContext">HttpActionExecutedContext object for the request.</param>
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Response != null)
            {
                string textToHash = DateTime.Now + Guid.NewGuid().ToString();
                string cookieText = HelperModule.GetHash(textToHash, CsrfTokenSalt);

                /* Make sure the clientside can see the passed in header. */
                actionExecutedContext.Response.Headers.Add("Access-Control-Expose-Headers", "Set-Cookie");

                var cookie = new CookieHeaderValue(XsrfCookieName, HttpUtility.UrlEncode(cookieText));
                cookie.Expires = DateTimeOffset.Now.AddDays(1);
                cookie.Secure = true; // cookie can be used on HTTPS only.
                cookie.HttpOnly = false; // javascript needs to be able to get this in order to pass it back in the headers in the next request

                if (actionExecutedContext.Request.RequestUri.Host == "localhost")
                {
                    cookie.Domain = null;
                }
                else
                {
                    cookie.Domain = actionExecutedContext.Request.RequestUri.Host;
                }

                cookie.Path = "/";

                actionExecutedContext.Response.Headers.AddCookies(new[] { cookie });
            }

            base.OnActionExecuted(actionExecutedContext);
        }
    }
}