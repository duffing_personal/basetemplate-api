﻿using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Cors;
using BaseTemplate.Auth.Filters;
using Newtonsoft.Json.Serialization;

namespace BaseTemplate.Auth
{
    /// <summary>
    ///  Standard configuration class for WebAPI.
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        ///  Method for setting up WebAPI configurations.
        /// </summary>
        /// <param name="config">The configuration object that settings will be applied to.</param>
        public static void Register(HttpConfiguration config)
        {
            /* Standard WebAPI routing. */
            config.MapHttpAttributeRoutes();

            /* Routing for authentication layer. */
            config.Routes.MapHttpRoute("AuthenticationApi", "auth/{controller}/{id}", new {id = RouteParameter.Optional});

            /* Routing for resources layer. */
            config.Routes.MapHttpRoute("ResourcesApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });

            /* Custom global filter applied for CSRF protection. */
            config.Filters.Add(new ValidateAntiForgeryToken());

            /* Sets up all JSON object properties to return in camel case. We do this because .NET properties will return properties as pascal cased by default. */
            JsonMediaTypeFormatter jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}