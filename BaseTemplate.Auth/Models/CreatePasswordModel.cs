﻿namespace BaseTemplate.Auth.Models
{
    /// <summary>
    ///  Model used for creating a password for an account that didn't already have a password (should only happens with external provider accounts).
    /// </summary>
    public class CreatePasswordModel
    {
        /// <summary>
        ///  Confirmation of password user wants for their account.
        /// </summary>
        public string confirmPassword { get; set; }

        /// <summary>
        ///  Password user wants for their account.
        /// </summary>
        public string password { get; set; }
    }
}