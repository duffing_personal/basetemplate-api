﻿namespace BaseTemplate.Auth.Models
{
    /// <summary>
    ///  Model used for helping in validation of external access tokens.
    /// </summary>
    public class ParsedExternalAccessTokenModel
    {
        /// <summary>
        ///  ID of the app for validation.
        /// </summary>
        public string app_id { get; set; }

        /// <summary>
        ///  ID of the user for validation.
        /// </summary>
        public string user_id { get; set; }
    }
}