﻿using System.ComponentModel.DataAnnotations;

namespace BaseTemplate.Auth.Models
{
    /// <summary>
    ///  Model used for creation of a user account.
    /// </summary>
    public class UserModel
    {
        /// <summary>
        ///  Confirmation of password potential user desires.
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        ///  Email of the user.
        /// </summary>
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        /// <summary>
        ///  Password that the user wants for their account.
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        ///  Username that the user wants for their account.
        /// </summary>
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }
    }
}