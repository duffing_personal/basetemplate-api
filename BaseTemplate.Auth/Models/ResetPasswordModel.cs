﻿namespace BaseTemplate.Auth.Models
{
    /// <summary>
    ///  Model that's used for resetting a user's password.
    /// </summary>
    public class ResetPasswordModel
    {
        /// <summary>
        ///  Password that the user wants.
        /// </summary>
        public string password { get; set; }

        /// <summary>
        ///  Confirmation of password that the user wants.
        /// </summary>
        public string confirmPassword { get; set; }

        /// <summary>
        ///  The token that's used to validate that the user is able to reset their password without entering the old password first.
        /// </summary>
        public string token { get; set; }

        /// <summary>
        ///  ID of the user we're resetting a password for.
        /// </summary>
        public int userId { get; set; }
    }
}