﻿using System.ComponentModel.DataAnnotations;

namespace BaseTemplate.Auth.Models
{
    /// <summary>
    ///  Model for registering an external provider's user to the application.
    /// </summary>
    public class RegisterExternalBindingModel
    {
        /// <summary>
        ///  Token issued by the external provider.
        /// </summary>
        [Required]
        public string ExternalAccessToken { get; set; }

        /// <summary>
        ///  Name of the external provider.
        /// </summary>
        [Required]
        public string Provider { get; set; }

        /// <summary>
        ///  Username the user wants for their local account.
        /// </summary>
        [Required]
        public string UserName { get; set; }

        /// <summary>
        ///  Email of the user.
        /// </summary>
        public string Email { get; set; }
    }
}