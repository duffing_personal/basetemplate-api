﻿using System;
using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace BaseTemplate.Auth.Models
{
    /// <summary>
    ///  Helps with retrieving external login data.
    /// </summary>
    public class ExternalLoginData
    {
        /// <summary>
        ///  Token issued by external provider.
        /// </summary>
        public string ExternalAccessToken { get; set; }

        /// <summary>
        ///  Name of the external login provider.
        /// </summary>
        public string LoginProvider { get; set; }

        /// <summary>
        ///  Key of the external login provider.
        /// </summary>
        public string ProviderKey { get; set; }

        /// <summary>
        ///  Username of the user.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        ///  Email of the user.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///  Issues a 302 redirect to the "redirect uri" set by the client application.
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
        {
            if (identity == null)
            {
                return null;
            }

            Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

            if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer) || String.IsNullOrEmpty(providerKeyClaim.Value))
            {
                return null;
            }

            if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
            {
                return null;
            }

            return new ExternalLoginData
            {
                LoginProvider = providerKeyClaim.Issuer,
                ProviderKey = providerKeyClaim.Value,
                UserName = identity.FindFirstValue(ClaimTypes.Name),
                ExternalAccessToken = identity.FindFirstValue("ExternalAccessToken"),
                Email = identity.FindFirstValue(ClaimTypes.Email)
            };
        }
    }
}