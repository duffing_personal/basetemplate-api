﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security.Google;

namespace BaseTemplate.Auth.Providers
{
    /// <summary>
    ///  Class for Google as an external provider.
    /// </summary>
    public class GoogleAuthProvider : IGoogleOAuth2AuthenticationProvider
    {
        /// <summary>
        ///  Called when a Challenge causes a redirect to authorize endpoint in Google OpenID middleware.
        /// </summary>
        /// <param name="context">Contains redirect URI and AuthenticationProperties of the challenge.</param>
        public void ApplyRedirect(GoogleOAuth2ApplyRedirectContext context)
        {
            context.Response.Redirect(context.RedirectUri);
        }

        /// <summary>
        ///  Allows us to read the external claims set by Google. Invoked whenever Google succesfully authenticates a user
        /// </summary>
        /// <param name="context">GoogleOAuth2AuthenticationContext object sent by Google. Contains information about the login session as well as the user ClaimsIdentity.</param>
        /// <returns>A Task representing the completed operation.</returns>
        public Task Authenticated(GoogleOAuth2AuthenticatedContext context)
        {
            context.Identity.AddClaim(new Claim("ExternalAccessToken", context.AccessToken));
            return Task.FromResult<object>(null);
        }

        /// <summary>
        ///  Invoked prior to the ClaimsIdentity being saved in a local cookie and the browser being redirected to the originally requested URL.
        /// </summary>
        /// <param name="context">Contains information about the login session as well as the user ClaimsIdentity.</param>
        /// <returns>A Task representing the completed operation.</returns>
        public Task ReturnEndpoint(GoogleOAuth2ReturnEndpointContext context)
        {
            return Task.FromResult<object>(null);
        }
    }
}