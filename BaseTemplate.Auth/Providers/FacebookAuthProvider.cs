﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security.Facebook;

namespace BaseTemplate.Auth.Providers
{
    /// <summary>
    ///  Class for Facebook as an external provider.
    /// </summary>
    public class FacebookAuthProvider : FacebookAuthenticationProvider
    {
        /// <summary>
        ///  Allows us to read the external claims set by Facebook. Invoked whenever Facebook succesfully authenticates a user.
        /// </summary>
        /// <param name="context">FacebookAuthenticationContext object sent by Facebook. Contains information about the login session as well as the user ClaimsIdentity.</param>
        /// <returns>A Task representing the completed operation.</returns>
        public override Task Authenticated(FacebookAuthenticatedContext context)
        {
            context.Identity.AddClaim(new Claim("ExternalAccessToken", context.AccessToken));
            return Task.FromResult<object>(null);
        }
    }
}