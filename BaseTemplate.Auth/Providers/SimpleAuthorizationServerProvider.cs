﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using BaseTemplate.Auth.Identity;
using BaseTemplate.Auth.Modules;
using BaseTemplate.Auth.Repositories;
using BaseTemplate.Entities.DataEntities;
using BaseTemplate.Entities.IdentityEntities;
using BaseTemplate.Entities.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;

namespace BaseTemplate.Auth.Providers
{
    /// <summary>
    ///  Validates the credentials for users asking for tokens.
    /// </summary>
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        /// <summary>
        ///  Called when a request to the token endpoint arrives with a "grant_type" of "refresh_token".
        /// </summary>
        /// <param name="context">The context of the event carries information in and results out.</param>
        /// <returns>Task to enable asynchronous execution.</returns>
        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            string originalClient = context.Ticket.Properties.Dictionary["as:client_id"];
            string currentClient = context.ClientId;

            if (originalClient != currentClient)
            {
                context.SetError("invalid_clientId", "Refresh token is issued to a different clientId.");
                return Task.FromResult<object>(null);
            }

            // Change auth ticket for refresh token requests
            ClaimsIdentity newIdentity = new ClaimsIdentity(context.Ticket.Identity);

            AuthenticationTicket newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }

        /// <summary>
        ///  Responsible for validating the username and password combination sent to the auth layer's token endpoint.
        /// </summary>
        /// <param name="context">The context of the event carries information in and results out.</param>
        /// <returns>Task to enable asynchronous execution.</returns>
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            UserManager<Account, int> _userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();

            ClaimsIdentity identity = new ClaimsIdentity();

            var header = context.OwinContext.Response.Headers.SingleOrDefault(h => h.Key == "Access-Control-Allow-Origin");
            if (header.Equals(default(KeyValuePair<string, string[]>)))
            {
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            }

            context.OwinContext.Response.Headers.Set("Access-Control-Allow-Origin", "*");
            Account user;
            try
            {
                user = await _userManager.FindAsync(context.UserName, context.Password);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            if (!user.EmailConfirmed)
            {
                /* Generate confirmation resend email link */
                context.Response.Headers.Add("Access-Control-Expose-Headers", new [] { "callbackData", "callbackText", "callbackMethod", "callbackService" });

                context.SetError("invalid_grant", "Email isn't confirmed.");
                context.Response.Headers.Add("callbackMethod", new[] {"sendConfirmationEmail"});
                context.Response.Headers.Add("callbackService", new[] {"authService"});
                context.Response.Headers.Add("callbackData", new[] {user.UserName});
                context.Response.Headers.Add("callbackText", new[] {"Resend confirmation email"});

                return;
            }

            identity = await _userManager.CreateIdentityAsync(user, OAuthDefaults.AuthenticationType);

            string allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");

            if (allowedOrigin == null)
            {
                allowedOrigin = "*";
            }

            context.OwinContext.Response.Headers.Set("Access-Control-Allow-Origin", allowedOrigin);

            AuthenticationProperties props = new AuthenticationProperties(new Dictionary<string, string>
            {
                {
                    "as:client_id", context.ClientId ?? string.Empty
                },
                {
                    "userName", context.UserName
                }
            });

            AuthenticationTicket ticket = new AuthenticationTicket(identity, props);
            context.Validated(ticket);
        }

        /// <summary>
        ///  Called at the final stage of a successful token endpoint request.
        /// </summary>
        /// <param name="context">The context of the event carries information in and results out.</param>
        /// <returns>Task to enable asynchronous execution.</returns>
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            var test = Task.FromResult<object>(null);

            

            return test;
        }

        /// <summary>
        ///  Validates that the origin of the request is a registered client.
        /// </summary>
        /// <param name="context">The context of the event carries information in and results out.</param>
        /// <returns>Task to enable asynchronous execution.</returns>
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId = string.Empty;
            string clientSecret = string.Empty;
            Client client = null;

            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }

            if (context.ClientId == null)
            {
                context.SetError("invalid_clientId", "ClientId should be sent.");
                return Task.FromResult<object>(null);
            }

            using (AuthRepository _repo = new AuthRepository(HttpContext.Current.GetOwinContext()))
            {
                client = _repo.FindClient(context.ClientId);
            }

            if (client == null)
            {
                context.SetError("invalid_clientId", string.Format("Client '{0}' is not registered in the system.", context.ClientId));
                return Task.FromResult<object>(null);
            }

            if (client.ApplicationType == ApplicationTypes.NativeConfidential)
            {
                if (string.IsNullOrWhiteSpace(clientSecret))
                {
                    context.SetError("invalid_clientId", "Client secret should be sent.");
                    return Task.FromResult<object>(null);
                }
                if (client.Secret != HelperModule.GetHash(clientSecret))
                {
                    context.SetError("invalid_clientId", "Client secret is invalid.");
                    return Task.FromResult<object>(null);
                }
            }

            if (!client.Active)
            {
                context.SetError("invalid_clientId", "Client is inactive.");
                return Task.FromResult<object>(null);
            }

            context.OwinContext.Set("as:clientAllowedOrigin", client.AllowedOrigin);
            context.OwinContext.Set("as:clientRefreshTokenLifeTime", client.RefreshTokenLifeTime.ToString());

            context.Validated();
            return Task.FromResult<object>(null);
        }
    }
}