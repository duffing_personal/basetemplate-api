﻿using System;
using System.IdentityModel.Tokens;
using System.Web;
using BaseTemplate.Auth.Repositories;
using BaseTemplate.Entities.DataEntities;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Thinktecture.IdentityModel.Tokens;

namespace BaseTemplate.Auth.Providers
{
    /// <summary>
    ///  Class responsible for generating JWT access tokens.
    /// </summary>
    public class SimpleJwtProvider : ISecureDataFormat<AuthenticationTicket>
    {
        /// <summary>
        ///  Where the client value is stored.
        /// </summary>
        private const string ClientPropertyKey = "as:client_id";

        /// <summary>
        ///  Global variable for storing issuer data for this class.
        /// </summary>
        private readonly string _issuer = string.Empty;

        /// <summary>
        ///  Standard constructor for the object.
        /// </summary>
        /// <param name="issuer">Issuer of this JWT, which would be the auth layer uri.</param>
        public SimpleJwtProvider(string issuer)
        {
            _issuer = issuer;
        }

        /// <summary>
        ///  Reads the client, the key for the client, and encodes a JSON web token using this key.
        /// </summary> 
        /// <param name="data">AuthenticationTicket object for this class.</param>
        /// <returns>An encoded JSON web token with the claims.</returns>
        public string Protect(AuthenticationTicket data)
        {
            Client client = null;

            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            string clientId = data.Properties.Dictionary.ContainsKey(ClientPropertyKey) ? data.Properties.Dictionary[ClientPropertyKey] : null;

            if (string.IsNullOrWhiteSpace(clientId)) throw new InvalidOperationException("AuthenticationTicket.Properties does not include client.");

            using (AuthRepository _repo = new AuthRepository(HttpContext.Current.GetOwinContext()))
            {
                client = _repo.FindClient(clientId);
            }

            string symmetricKeyAsBase64 = client.Secret;

            var keyByteArray = TextEncodings.Base64Url.Decode(symmetricKeyAsBase64);

            var signingKey = new HmacSigningCredentials(keyByteArray);

            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            var token = new JwtSecurityToken(_issuer, clientId, data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, signingKey);

            var handler = new JwtSecurityTokenHandler();

            var jwt = handler.WriteToken(token);

            return jwt;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <param name="protectedText"></param>
        /// <returns></returns>
        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}