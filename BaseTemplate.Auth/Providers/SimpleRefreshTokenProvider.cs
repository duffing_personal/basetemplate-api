﻿using System;
using System.Threading.Tasks;
using System.Web;
using BaseTemplate.Auth.Modules;
using BaseTemplate.Auth.Repositories;
using BaseTemplate.Entities.DataEntities;
using Microsoft.Owin.Security.Infrastructure;

namespace BaseTemplate.Auth.Providers
{
    /// <summary>
    ///  Generates refresh tokens and stores them in the database.
    /// </summary>
    public class SimpleRefreshTokenProvider : IAuthenticationTokenProvider
    {
        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///  Contains the generation logic for the refresh tokens.
        /// </summary>
        /// <param name="context">The context of the event carries information in and results out.</param>
        /// <returns>Task to enable asynchronous execution.</returns>
        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            string clientid = context.Ticket.Properties.Dictionary["as:client_id"];

            if (string.IsNullOrEmpty(clientid))
            {
                return;
            }

            /* Generating a unique identifier for the refresh token. */
            string refreshTokenId = Guid.NewGuid().ToString("n");

            using (AuthRepository _repo = new AuthRepository(HttpContext.Current.GetOwinContext()))
            {
                string refreshTokenLifeTime = context.OwinContext.Get<string>("as:clientRefreshTokenLifeTime");

                RefreshToken token = new RefreshToken
                {
                    Id = HelperModule.GetHash(refreshTokenId),
                    ClientId = clientid,
                    Subject = context.Ticket.Identity.Name,
                    IssuedUtc = DateTime.UtcNow,
                    ExpiresUtc = DateTime.UtcNow.AddMinutes(Convert.ToDouble(refreshTokenLifeTime))
                };

                context.Ticket.Properties.IssuedUtc = token.IssuedUtc;
                context.Ticket.Properties.ExpiresUtc = token.ExpiresUtc;

                token.ProtectedTicket = context.SerializeTicket();

                bool result = await _repo.AddRefreshToken(token);

                if (result)
                {
                    context.SetToken(refreshTokenId);
                }
            }
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            string allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");
            context.OwinContext.Response.Headers.Set("Access-Control-Allow-Origin", allowedOrigin);

            string hashedTokenId = HelperModule.GetHash(context.Token);

            using (AuthRepository _repo = new AuthRepository(HttpContext.Current.GetOwinContext()))
            {
                RefreshToken refreshToken = await _repo.FindRefreshToken(hashedTokenId);

                if (refreshToken != null)
                {
                    /* Get protectedTicket from refreshToken class */
                    context.DeserializeTicket(refreshToken.ProtectedTicket);
                    bool result = await _repo.RemoveRefreshToken(hashedTokenId);
                }
            }
        }
    }
}