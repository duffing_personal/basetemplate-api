﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Owin.Security;

namespace BaseTemplate.Auth.Results
{
    /// <summary>
    ///  Responsible for passing on the name of the external provider being used.
    /// </summary>
    public class ChallengeResult : IHttpActionResult
    {
        /// <summary>
        ///  Used for XSRF protection when adding external logins
        /// </summary>
        private const string XsrfKey = "XsrfId";

        /// <summary>
        ///  Name of the external provider.
        /// </summary>
        public string LoginProvider { get; set; }

        /// <summary>
        ///  The URI constructed that's returned.
        /// </summary>
        public string RedirectUri { get; set; }

        /// <summary>
        ///  The request from the client side.
        /// </summary>
        public HttpRequestMessage Request { get; set; }

        /// <summary>
        ///  Stored as the xsrf key.
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        ///  Standard constructor.
        /// </summary>
        /// <param name="loginProvider">Name of external provider.</param>
        /// <param name="controller">Controller calling this class.</param>
        /// <param name="redirectUri">The redirect URI.</param>
        /// <param name="userId">User ID calling this class.</param>
        public ChallengeResult(string loginProvider, ApiController controller = null, string redirectUri = null, int? userId = null)
        {
            LoginProvider = loginProvider;

            if (controller != null)
            {
                Request = controller.Request;
            }

            RedirectUri = redirectUri;
            UserId = userId;
        }

        /// <summary>
        ///  Creates an HttpResponseMessage asynchronously.
        /// </summary>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        /// <returns>A task that, when completed, contains the HttpResponseMessage.</returns>
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            if (UserId != null)
            {
                AuthenticationProperties properties = new AuthenticationProperties {RedirectUri = RedirectUri};
                properties.Dictionary[XsrfKey] = Convert.ToString(UserId);

                Request.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
            else
            {
                Request.GetOwinContext().Authentication.Challenge(LoginProvider);
            }

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            response.RequestMessage = Request;
            return Task.FromResult(response);
        }
    }
}