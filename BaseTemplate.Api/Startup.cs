﻿using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Jwt;
using Owin;

[assembly: OwinStartup(typeof(BaseTemplate.Api.Startup))]
namespace BaseTemplate.Api
{
    /// <summary>
    ///  Standard Owin Startup class.
    /// </summary>
    public class Startup
    {
        /// <summary>
        ///  Configures the settings for the application.
        /// </summary>
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();
            ConfigureOAuth(app);

            WebApiConfig.Register(config);
            app.UseWebApi(config);

        }

        /// <summary>
        ///  Configures the OAuth settings for the application.
        /// </summary>
        private void ConfigureOAuth(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);

            /* Api controllers with an [Authorize] attribute will be validated with JWT */
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] { Properties.Settings.Default.ClientID },
                    IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new SymmetricKeyIssuerSecurityTokenProvider(Properties.Settings.Default.SiteURL, Properties.Settings.Default.ClientSecret)
                    }
                });
        }
    }
}