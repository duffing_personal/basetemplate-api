﻿using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using BaseTemplate.Entities.DataEntities;
using Microsoft.Data.Edm;
using Newtonsoft.Json.Serialization;

namespace BaseTemplate.Api
{
    /// <summary>
    ///  Standard configuration class for WebAPI.
    /// </summary>
    public class WebApiConfig
    {
        /// <summary>
        ///  Method for setting up WebAPI configurations.
        /// </summary>
        /// <param name="config">The configuration object that settings will be applied to.</param>
        public static void Register(HttpConfiguration config)
        {
            /* Web API routes */
            config.MapHttpAttributeRoutes();
            config.Routes.MapODataServiceRoute("odata", null, GenerateEdmModel());

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }

        /// <summary>
        ///  Creates the model for odata endpoints.
        /// </summary>
        /// <returns>The IEdmModel for OData.</returns>
        private static IEdmModel GenerateEdmModel()
        {
            var builder = new ODataConventionModelBuilder();
            builder.EntitySet<Client>("clients");

            return builder.GetEdmModel();
        }
    }
}