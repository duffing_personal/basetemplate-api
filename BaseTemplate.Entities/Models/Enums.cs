﻿namespace BaseTemplate.Entities.Models
{
    /// <summary>
    ///  Enum that determines what type of application a client is.
    /// </summary>
    public enum ApplicationTypes
    {
        JavaScript = 0,
        NativeConfidential = 1
    };
}