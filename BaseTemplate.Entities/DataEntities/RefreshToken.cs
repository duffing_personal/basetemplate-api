﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BaseTemplate.Entities.DataEntities
{
    /// <summary>
    ///  Represents the RefreshToken table in the database. This table holds all of the current refresh tokens for all logged in users of the application.
    /// </summary>
    [Table("RefreshToken")]
    public class RefreshToken
    {
        /// <summary>
        ///  The ID of the client that this refresh token is associated with.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string ClientId { get; set; }

        /// <summary>
        ///  Expiration time of the refresh token.
        /// </summary>
        public DateTime ExpiresUtc { get; set; }

        /// <summary>
        ///  Primary key of the refresh token.
        /// </summary>
        [Key]
        public string Id { get; set; }

        /// <summary>
        ///  Time that the refresh token was issued.
        /// </summary>
        public DateTime IssuedUtc { get; set; }

        /// <summary>
        ///  Signed string which contains a serialized representation for the ticket for a specific user. 
        ///  It contains all the claims and ticket properties for the user.
        ///  Owin middleware uses this string to build the new access tokens.
        /// </summary>
        [Required]
        public string ProtectedTicket { get; set; }

        /// <summary>
        ///  Indicates to which user this refresh token belongs.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Subject { get; set; }

        /// <summary>
        ///  Last modified time for this refresh token.
        /// </summary>
        [Timestamp]
        public Byte[] TimeStamp { get; set; }
    }
}