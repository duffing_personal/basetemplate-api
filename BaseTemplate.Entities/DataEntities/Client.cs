﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BaseTemplate.Entities.Models;

namespace BaseTemplate.Entities.DataEntities
{
    /// <summary>
    ///  This class is a representation of the Clients table in the DB. This table determines what applications are allowed to call the API.
    /// </summary>
    [Table("Client")]
    public class Client
    {
        /// <summary>
        ///  A flag that controls if the client is allowed to call this API.
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        ///  URI of the client that is allowed to call the API.
        /// </summary>
        [MaxLength(100)]
        public string AllowedOrigin { get; set; }

        /// <summary>
        ///  The type of application the client represents.
        /// </summary>
        public ApplicationTypes ApplicationType { get; set; }

        /// <summary>
        ///  Primary key of the client.
        /// </summary>
        [Key]
        public string Id { get; set; }

        /// <summary>
        ///  Name of the client.
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        ///  How long a refresk token stays active for the client.
        /// </summary>
        public int RefreshTokenLifeTime { get; set; }

        /// <summary>
        ///  Hash that is used for encrypting and decrpyting JSON Web Tokens for the client.
        /// </summary>
        [Required]
        public string Secret { get; set; }

        /// <summary>
        ///  When the client was last modified.
        /// </summary>
        [Timestamp]
        public Byte[] TimeStamp { get; set; }
    }
}