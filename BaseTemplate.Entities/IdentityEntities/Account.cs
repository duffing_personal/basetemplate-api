﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BaseTemplate.Entities.IdentityEntities
{
    /// <summary>
    ///  Class that inherits from ASP.NET Identity class for IdentityUser.
    /// </summary>
    public class Account : IdentityUser<int, AccountLogin, AccountRole, AccountClaim>
    {
        /// <summary>
        ///  Used for verifying that a request is coming from this account. This is a pseudo-password.
        /// </summary>
        public String HashedSecurityToken { get; set; }

        /// <summary>
        ///  Last modified time for this account.
        /// </summary>
        [Timestamp]
        public Byte[] TimeStamp { get; set; }
    }
}