﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BaseTemplate.Entities.IdentityEntities
{
    /// <summary>
    ///  Class that inherits from ASP.NET Identity class for IdentityRole.
    /// </summary>
    public class Role : IdentityRole<int, AccountRole>
    {
        /// <summary>
        ///  Last modified time for this Role.
        /// </summary>
        [Timestamp]
        public Byte[] TimeStamp { get; set; }
    }
}