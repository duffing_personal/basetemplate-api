﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BaseTemplate.Entities.IdentityEntities
{
    /// <summary>
    ///  Class that inherits from ASP.NET Identity class for IdentityUserRole.
    /// </summary>
    public class AccountRole : IdentityUserRole<int>
    {
        /// <summary>
        ///  Last modified time for this AccountRole.
        /// </summary>
        [Timestamp]
        public Byte[] TimeStamp { get; set; }
    }
}