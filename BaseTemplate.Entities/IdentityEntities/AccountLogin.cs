﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BaseTemplate.Entities.IdentityEntities
{
    /// <summary>
    ///  Class that inherits from the ASP.NET Identity class IdentityUserLogin.
    /// </summary>
    public class AccountLogin : IdentityUserLogin<int>
    {
        /// <summary>
        ///  Last modified time for this AccountLogin.
        /// </summary>
        [Timestamp]
        public Byte[] TimeStamp { get; set; }
    }
}