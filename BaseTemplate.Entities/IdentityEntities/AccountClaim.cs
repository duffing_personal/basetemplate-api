﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BaseTemplate.Entities.IdentityEntities
{
    /// <summary>
    ///  Class that inherits from the ASP.NET Identity class IdentityUserClaim. 
    /// </summary>
    public class AccountClaim : IdentityUserClaim<int>
    {
        /// <summary>
        ///  Last modified time for this AccountClaim.
        /// </summary>
        [Timestamp]
        public Byte[] TimeStamp { get; set; }
    }
}